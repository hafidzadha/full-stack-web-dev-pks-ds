<?php
 
 abstract class Hewan {
     public $nama;
     public $keahlian;     
     public $jumlahKaki;
     public $darah = 50;

    /*public function __construct($nama , $keahlian, $jumlahKaki, $darah = 50)
     {
        $this->nama = $nama;
        $this->keahlian = $keahlian;
        $this->jumlahKaki = $jumlahKaki;
        $this->darah = $darah;
     } 

    function tampilan()
     {
        return "Hewan ini bernama " . $this->nama . " , memiliki keahlian " . $this->keahlian . " , dengan jumlah kaki " . $this->jumlahKaki . " , dan jumlah darah " . $this->darah;
     } 
     */

     abstract public function getInfoHewan();

     public function setNama($nama){
         $this->nama = $nama;
     }

     public function setKeahlian($keahlian){
         $this->keahlian = $keahlian;
     }

     public function setJumlahKaki($jumlahKaki){
        $this->jumlahKaki = $jumlahKaki;
    }


    public function getNama()
    {
        echo "Nama Hewan : " . $this->nama;
    }

    public function getKeahlian()
    {
        echo "Keahlian : " . $this->keahlian;
    }

    public function getJumlahKaki()
    {
        echo "Jumlah Kaki : " . $this->jumlahKaki;
    }

    public function getDarah()
    {
        echo "Jumlah Darah : " . $this->darah;
    }

    public static function atraksi($nama , $keahlian){
        echo $nama . " sedang " . $keahlian;
    }

 }

 abstract class Fight extends Hewan{
     public $attackPower;
     public $defencePower;

     abstract public function getInfoHewan();

     public function setAttackPower($attackPower){
        $this->attackPower = $attackPower;
    }

    public function setDefencePower($defencePower){
        $this->defencePower = $defencePower;
    }


    public function getAttackPower()
    {
        echo "Attack Power = " . $this->attackPower;
    }

    public function getDefencePower()
    {
        echo "Defence Power = " . $this->defencePower;
    }

    public function serang($hewan1, $hewan2){
        echo $hewan1 . " sedang menyerang " . $hewan2 ;
    }

    public function diserang($hewan1, $hewan2, $darah, $attackPower, $defencePower){
        echo $hewan2 . " diserang " . $hewan1 . " darah sekarang " . $darah . " - attack Power penyerang = " . $attackPower . " defence Power diserang = " . $defencePower;
    }

    function panggilSerang($hewan1, $hewan2, $darah, $attackPower, $defencePower){
        echo $hewan1 . " sedang menyerang " . $hewan2  . " ," . $hewan2 . " diserang " . $hewan1 . " darah sekarang " . $darah . " - attack Power penyerang = " . $attackPower . " defence Power diserang = " . $defencePower;
    }
 }

 class Elang extends Fight{
     public function getInfoHewan(){
         return ;
     }
 }

 class Harimau extends Fight{
    public function getInfoHewan(){

    }
 }

$elang1 = new Elang;
$elang1->setNama("Elang");
$elang1->getNama("Elang");
echo "<br>";
$elang1->setJumlahKaki("2");
$elang1->getJumlahKaki("2");
echo "<br>";
$elang1->setKeahlian("terbang tinggi");
$elang1->getKeahlian("terbang tinggi");
echo "<br>";
$elang1->setAttackPower("10");
$elang1->getAttackPower("10");
echo "<br>";
$elang1->setDefencePower("5");
$elang1->getDefencePower("5");

echo "<br>";
echo "<br>";

$harimau1 = new Harimau;
$harimau1->setNama("Harimau");
$harimau1->getNama("Harimau");
echo "<br>";
$harimau1->setJumlahKaki("4");
$harimau1->getJumlahKaki("4");
echo "<br>";
$harimau1->setKeahlian("lari cepat");
$harimau1->getKeahlian("lari cepat");
echo "<br>";
$harimau1->setAttackPower("7");
$harimau1->getAttackPower("7");
echo "<br>";
$harimau1->setDefencePower("8");
$harimau1->getDefencePower("8");

echo "<br>";
echo "<br>";
echo Hewan::atraksi("Harimau", "lari cepat");
echo "<br>";
echo Hewan::atraksi("Elang", "terbang tinggi");

echo "<br>";
echo "<br>";
echo Fight::serang("Elang", "Harimau");
echo "<br>";
echo Fight::diserang("Elang", "Harimau", 2, 10, 8);
echo "<br>";
echo Fight::panggilSerang("Elang", "Harimau", 2, 10, 8);
//construct
/*
$harimau = new Hewan("Harimau", "berlari cepat", 4,);
echo $harimau->tampilan();
*/
?>